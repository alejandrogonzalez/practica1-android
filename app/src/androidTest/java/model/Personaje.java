package model;

public class Personaje {

    private String name;
    private String species;
    private String ancestry;
    private String patronous;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getAncestry() {
        return ancestry;
    }

    public void setAncestry(String ancestry) {
        this.ancestry = ancestry;
    }

    public String getPatronous() {
        return patronous;
    }

    public void setPatronous(String patronous) {
        this.patronous = patronous;
    }

}
