package adapter;

import android.widget.AdapterView;

import androidx.annotation.NonNull;

import java.util.List;

import model.Personaje;

public class PersonajeAdapter {

    private List<Personaje>personajes;
    private AdapterView.OnItemClickListener itemClickListener;

    public PersonajeAdapter(List<Personaje>personajes, AdapterView.OnItemClickListener itemClickListener){
        this.personajes = personajes;
        this.itemClickListener = itemClickListener;
    }

    public void setPersonajes(List<Personaje>personajes){
        this.personajes = personajes;

    }

    public void onBindViewHolder(@NonNull PersonajeAdapter.PersonajeAdapterHolder holder, int position){

    }


    public class PersonajeAdapterHolder {
    }
}
